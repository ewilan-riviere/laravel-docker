FROM mysql:8.0

ENV MYSQL_ROOT_PASSWORD=secret
ENV MYSQL_DATABASE=notify
ENV MYSQL_USER=notify
ENV MYSQL_PASSWORD=secret

FROM php:8.2

ENV COMPOSER_ALLOW_SUPERUSER=1

WORKDIR /app

RUN apt update && apt install -y git bash curl openssh-client rsync tar unzip zip python3 python3-pip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions
RUN install-php-extensions gd exif intl pdo_mysql zip opcache bcmath pcntl sockets

RUN git clone https://gitlab.com/ewilan-riviere/notify.git /app
RUN pip install discord-webhook python-dotenv
RUN cp .env.example .env
RUN ln -s /path/to/notify/notify /usr/local/bin
