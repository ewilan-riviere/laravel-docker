# Laravel docker

Available on [Docker Hub](https://hub.docker.com/r/ewilanriviere/laravel-docker).

## Pull

```bash
docker pull ewilanriviere/laravel-docker
```

## Build

```bash
docker build -t ewilanriviere/laravel-docker .
```

## Push to Docker Hub

```bash
docker push ewilanriviere/laravel-docker
```
